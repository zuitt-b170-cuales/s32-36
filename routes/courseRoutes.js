const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")


/*
	ACTIVITY
		create a route that will let an admin perform addCourse function in the courseController
			verify that the user is logged in
			decode the token for that user
			use the id, isAdmin, and request body to perform the function in courseController
*/

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the toke npayload (id, email, isAdmin fields of the user)
	courseController.addCourse(req.body, userData).then(result => res.send(result))
})


// eCommerce websites

/*
	create a route that will retrieve all of our products/courses will require login/register functions
*/

router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})


/*
	miniactivity
		create a route that will retrieve all of our products/courses 
			will not require login/register functions
*/



// retrieve all courses
module.exports.getAllCourses = () => {
	// empty ( {} ) looks for all documents
	returnCourse.find( {} ).then(result => {
		return result
	})
}

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

// retrieve all active courses
router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

/*
	in getting all of the documents, in case we need multiple of them, place the route with the criteria to find the method first before getting the one with params
*/


// retrieve a course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(result => res.send(result))
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

/*
	delete is never a norm in databases

	use /courseId/archiveCourse and send a PUT request to archive a course by changing the active status
*/

router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archivedCourse(req.params, req.body).then(result => res.send(result))
})

module.exports = router;