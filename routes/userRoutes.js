const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

/*
	Miniactivity
		Create a function with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController
	Reminder: We have a parameter needed in the checkEmail function and that is data from the request body
*/

// checking of email
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result));
})

// user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

// user login
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})	

// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js; called the middleware
router.get("/details", auth.verify, (req, res) => {
	// decode - decrypts the token inside the authorization ( which is in the headers of the request )
	// req.headers.authorization contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result))
})

/*
miniactivity
	create getProfile function inside the userController
		1. find the id of the user using "data" as parameters
		2. if it does not exist, return false
		3. otherwise, return true
*/

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
})	

/*
miniactivity
	create a post request under the '/enroll' endpoint that would allow the use of "enroll" function in the userController
	check if the user is logged in
	create a variable and store an object with userId and courseId as fields
		userId - decoded token of the user
		courseId - the course id present in the request body

	user the variable as a parameter of the enroll function
*/

module.exports = router;