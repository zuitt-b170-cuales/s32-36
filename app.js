const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

// access to routes
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

// server 
const app = express()
const port = 4000

app.use(cors()) // allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes)

mongoose.connect("mongodb+srv://owencuales:OplanSagittarius1972@wdc028-course-booking.3hxrc.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
	})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.listen(port, () => console.log(`Now listening to port 4000`))