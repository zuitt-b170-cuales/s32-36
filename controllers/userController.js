// set up dependencies
const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") // used to encrypt user passwords

// check if the email exists
/*
	1. check for the email in the database
	2. send the result as a response (with error handling)
*/

//it is conventional for the devs to use Boolean in sending return responses especially with the backend application 

module.exports.checkEmail = (requestBody) => {
	return User.find({email: requestBody.email}).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				return true
			}else{
				return false
			}
		}
	})
}

/*
	USER REGISTRATION
		1. create a new User with the information from the requestBody
		2. make sure that the password is encrypted
		3. save the new user
*/
// Q: module.exports functions like let??
// Q: where does .save save it to?

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
			// hashSync is a function of bcrypt that encrypts the password
		password: bcrypt.hashSync(reqBody.password, 10),
			// 10 here means 10 rounds or times that it runs the algorithm to the the reqBody.password, 10 security codes that are jammed into one password
				// max is 72 implementations of algorithm
				// 10 is already pretty secure
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// USER LOGIN
/*
	1. find if the email is existing in the database
	2. check if the password is correct
*/ 

module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null){
			return false
		}else{
			// compareSync function - used to comparea non-encrypted password to an encrypted password and returns a Boolean response depending on the result
			/*
				true - a token should be created since the user is existing and the password is correct
				false - the passwords do not match, thus a token should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// access - field
				// auth - imported auth.js
				// createAccessToken - a function inside the auth.js to create access tokens
				// .toObject() - a function that transforms the User into an object that can be used by our createAccessToken
			}else{
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if(result === null){
			return false
		}else{
			result.password = "";
			return result
		}
	})
}


/*
	use another await keyword to update the enrollees array in the course collection
		find the courseId from the requestBody
		push the userId of the enrollee in the enrollees array of the course
		update the document in the database

		if both push are successful, return true
		if both pushing are not successful, return false
*/



/*
	1. find the user/document in the database
	2. add the courseId to the user's enrollment array
	3. save the document in the database
*/

module.exports.enroll = async (data) => {
	// finding the user in the database
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// added the courseId to the user's enrollment array
		user.enrollment.push( {courseId: data.courseId} )
		// saving in the database
		return user.save().then((user, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	/*
		use another await keyword to update the enrollees array in the course collection
			find the courseId from the requestBody push the userId of the enrollee in the enrollees array
	*/
	/*
		1. find the user / document in the database
		2. 
		3. save
	*/
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId})
		return course.save().then((course, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	if(isUserUpdated && isCourseUpdated){
		return true
	}else{
		return false
	}
}

/*
	Deploying a server
		1. register in Heroku (https://signup.heroku.com/login)
		2. check if you have heroku in your device
		3. create a "Procfile"
			- it hsould have P
			- it should not have any file extensions
			- inserting the text "web: node app"
		4. login your heroku through the gitbash/terminal
			- "heroku login"
				press any key
				log in using your heroku
				there should be confirmation
		5. heroku create to create/deploy your server app to a host in the internet
		6. git push heroku master to push the updates in heroku
*/